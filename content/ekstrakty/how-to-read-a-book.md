---
title: M.J. Adler i C. van Doren - How to read a book 
showToc: true
date: 2023-12-31T12:30:00
aliases:
 - howtoreadabook
 - howtoread
 - htrab
---

Książka ta jest dedykowana osobom, które pragną poszerzyć swoje rozumienie świata poprzez lekturę książek.
Jest to praktyczny poradnik poświęcony sztuce czytania.
Autor dostarcza nam framework, który możemy wykorzystać nie tylko do czytania książek, ale także innych tekstów.
Czytanie długich prac stanowi wyzwanie, znacznie trudniejsze niż lektura krótszych form. 
Jednakże wszystkie zdobyte tu umiejętności czytania książek będą przydatne także podczas przeglądania innych tekstów.
Książka składa się z&nbsp;trzech głównych części:

* Wprowadzenie - ogólne refleksje na temat sztuki i&nbsp;czynności czytania.
* Omówienie zasad - wprowadza różne stopnie czytania oraz wyjaśnia zasady rządzące każdym z&nbsp;nich.
* Omówienie rodzajów tekstów - opisuje, jak stosować zdobyte zasady czytania i&nbsp;jak dostosować je do różnych rodzajów książek.

## Czynność i&nbsp;sztuka czytania

Możemy wyróżnić trzy podstawowe cele czytania:

* Czytanie dla przyjemności - stanowi najmniej obciążającą intelektualnie formę.
* Czytanie dla informacji - pozwala zdobywać konkretne wiadomości i&nbsp;dane, a&nbsp;także przyswajać nową wiedzę.
* Czytanie dla zrozumienia - najbardziej wymagająca forma czytania tekstu, nad którą będziemy się skupiać.

Czytanie dla zrozumienia jest formą nauki poprzez instrukcję, choć w&nbsp;trudniejszej od szkolnej wersji, gdyż jesteśmy sami ze sobą (z *nieobecnym nauczycielem*).
To my musimy zadawać pytania i&nbsp;znaleźć odpowiedzi poprzez pracę z&nbsp;tekstem, włożyć pewien wysiłek, by dane zagadnienie zrozumieć.
W&nbsp;trakcie czytania dla zrozumienia musimy kierować się czterema podstawowymi pytaniami:

1. *O&nbsp;czym jest książka?* -  Musimy odkryć temat przewodni książki oraz w&nbsp;jaki sposób autor go podzielił na podrzędne tematy.
2. *Co zostało powiedziane i&nbsp;w jaki sposób?* - warto uchwycić główne idee, tezy i&nbsp;argumenty.
3. *Czy książka jest prawdziwa w&nbsp;całości lub części?* - Odpowiedzi na to pytanie nie można udzielić, nie rozumiejąc wcześniej treści oraz kontekstu tekstu. Ważne jest wypracowanie własnej opinii. Znajomość zdania autora jest niewystarczająca.
4. *Co w&nbsp;związku z&nbsp;tym?* - Autor przekazuje nam informacje, a&nbsp;my jesteśmy odpowiedzialni za ocenę ich ważności i&nbsp;znaczenia.

Jednak sama znajomość tych pytań nie wystarczy, ważne jest także kształtowanie nawyku ich używania. 
Należy zadawać je w&nbsp;trakcie lektury i&nbsp;starannie na nie odpowiadać - jest to właśnie *sztuką czytania*.

## Stopnie umiejętności czytania

Wyróżniamy cztery stopnie umiejętności czytania: elementarne, przeglądowe, analityczne, i&nbsp;czytanie synoptyczne.
Przejście na wyższy poziom wiąże się z&nbsp;opanowaniem stopnia poprzedniego; osiągnięcie wyższego poziomu jest niemożliwe bez solidnych podstaw z&nbsp;poprzednich etapów.

Na poziomie czytania elementarnego, kształconego w&nbsp;szkole podstawowej, skupiamy się na rozpoznawaniu liter, słów, zdań, oraz nauce gramatyki i&nbsp;wymowy. 
Kluczowe pytania to m.in. Co znaczy dane słowo? Co znaczy dane zdanie? (bez głębszych interpretacji).

Przechodząc do czytania przeglądowego, naszym celem jest efektywne przeskanowanie tekstu, aby wydobyć jak najwięcej informacji w&nbsp;możliwie krótkim czasie, często ograniczonym np. do 15-30 minut. 
Po zakończeniu tego rodzaju czytania, powinniśmy być w&nbsp;stanie odpowiadać na pytania dotyczące rodzaju tekstu, jego struktury i&nbsp;podziału na sekcje.

Czytanie analityczne stanowi najbardziej wymagający poziom - czytanie i&nbsp;praca nad tekstem bez limitu czasowego.
Wymaga pełnego zrozumienia tekstu oraz zadawania wielu pytań dotyczących książki.

Ostatni stopień to czytanie synoptyczne, znane również jako czytanie porównawcze.
Polega na czytaniu jednej książki w&nbsp;kontekście innych tekstów, ich porównywaniu oraz analizowaniu wspólnych tematów i&nbsp;treści.

## Czytanie elementarne

W&nbsp;procesie nauki czytania wyróżniamy cztery etapy:

1. (0 - 6 lat) Gotowość do nauki czytania - umiejętności przyszłego czytelnika, które stanowią podstawę nauki czytania. 
W&nbsp;tej grupie umiejętności znajdują się: dobry wzrok, zdolności intelektualne (jak umiejętność zapamiętywania znaków), umiejętności językowe, a&nbsp;także umiejętności interpersonalne, takie jak zdolność do współpracy z&nbsp;innymi dziećmi, koncentracja uwagi i&nbsp;zdolność do słuchania poleceń. 
Próba nauki czytania przez dziecko, które nie jest na to jeszcze gotowe, może prowadzić do frustracji dziecka i&nbsp;zrażenia go do samego procesu czytania.

2. (klasa 1) Nauka czytania bardzo prostych materiałów - począwszy od pojedynczych słów i&nbsp;zdań, aż do krótkich, prostych tekstów. 
Dziecko przechodzi z&nbsp;etapu niezrozumiałych znaków na papierze do nadawania im sensu. 

3. (klasa 1-4) Dynamiczny wzrost słownictwa i&nbsp;zdolność rozumienia znaczenia nieznanych słów przy użyciu wskazówek kontekstowych. 
Dzieci uczą się również, że czytanie może mieć różne cele (nauczanie, rozrywka itp.), a&nbsp;także zaczynają czytać teksty z&nbsp;różnych dziedzin, takie jak historia, nauka, literatura piękna itp.

4. (klasa 4+) Doskonalenie już nabytych umiejętności - uczymy się wychwytywać pojęcia zawarte w&nbsp;tekstach, porównywać poglądy różnych autorów na dany temat. 
Zazwyczaj ten etap osiągamy w&nbsp;wieku nastoletnim.

Te cztery etapy stanowią pierwszy poziom umiejętności czytania - czytanie elementarne.

## Czytania przeglądowe

Czytanie przeglądowe jest kluczowym etapem, pozwalającym nam znaleźć książki, które zasługują na bardziej szczegółowe zbadanie.
Ten rodzaj czytania składa się z&nbsp;dwóch głównych etapów:

* Systematyczne przeglądanie,
* Czytanie pobieżne (opcjonalny).

### Systematyczne przeglądanie

Załóżmy, że mamy przed sobą książkę:

* której treści nie znamy,
* nie jesteśmy pewni, czy warto ją przeczytać,
* podejrzewamy, że jest ona wartościowa,
* mamy ograniczony czas, by sprawdzić, czy gra jest warta świeczki.

Systematyczne przeglądanie jest idealnym narzędziem, które pomaga nam zrozumieć, czy dana książka jest godna uwagi.
Pomoże *odsiać ziarna od plew*.

Podczas systematycznego przeglądania przeprowadzamy następujące kroki:

1. **Spójrz na tytuł oraz wstęp lub przedmowę.** Staraj się zrozumieć zakres i&nbsp;cel książki oraz jak autor podchodzi do tematu.
2. **Przejrzyj spis treści** - odkryj strukturę książki i&nbsp;użyj go jak mapy przed podróżą.
3. **Zajrzyj do indeksu, aby poznać zakres omawianego materiału.** Jeśli natkniesz się na terminy lub zagadnienia, które wydają się interesujące, przeczytaj kilka fragmentów, które się do nich odnoszą.
4. **Przeczytaj notkę reklamową.**
5. **Przeskanuj kluczowe rozdziały książki** (te, które wydają się istotne). Jeśli zawierają podsumowania, przeczytaj je.
6. **Przeczytaj tu i&nbsp;ówdzie kilka zdań, paragrafów czy stron.** Jeśli książka ma epilog, przeczytaj kilka ostatnich stron.

Cały ten proces powinien zająć Ci około godziny.
Po jego zakończeniu będziesz wiedział, czy książka zasługuje na dalszą uwagę.
Na tym etapie bawimy się w&nbsp;detektywa, który szuka poszlak o&nbsp;temacie książki i&nbsp;ideach w&nbsp;niej zawartych. 
Musisz być wyczulony na każdą poszlakę, która pomoże rzucić światło na te kwestie.

### Czytanie pobieżne

Jeśli natkniesz się na książkę, która wydaje się trudna do zrozumienia, powinieneś ją przeczytać w&nbsp;sposób pobieżny. 
Oznacza to, że nie musisz zatrzymywać się nad treściami, które są dla Ciebie niezrozumiałe, nie przejmuj się przypisami, notkami czy trudnymi słowami.
Skup się na ogólnym przekazie i&nbsp;niezrozumiane fragmenty pozostaw na później.
Przeczytanie książki i&nbsp;zrozumienie jej w&nbsp;50% czy mniej, może pomóc Ci zrozumieć trudniejsze fragmenty w&nbsp;przyszłości.
Nawet jeśli nie wrócisz już do książki, lepiej jest zrozumieć jej część niż całkowicie ją zignorować.

## Czytanie analityczne
Proces czytania analitycznego składa się z&nbsp;trzech kluczowych etapów:
* Odkrywanie, o&nbsp;czym jest książka (zasady 1-4).
* Interpretacja treści (zasady 5-8).
* Krytyka książki (zasady 9 - 15).

### Zasady czytania analitycznego

**1. Sklasyfikuj książkę ze względu na jej rodzaj i&nbsp;tematykę.** 
Tytuł i&nbsp;wstęp są szczególnie ważne podczas kategoryzacji książek. Na przykład, tytuł *"The Decline and Fall of the Roman Empire"* (*"Zmierzch Cesarstwa Rzymskiego"*) wskazuje na tematykę historii Cesarstwa Rzymskiego od jego szczytu do upadku.
Autor często dostarcza w&nbsp;tytule lub wstępie kluczowych wskazówek dotyczących kategorii, do której należy książka.

Przeczytanie samego tytułu nie wystarczy, zwłaszcza jeśli nie masz szerokiego wachlarza kategorii, które możesz przypisać książce.
Na przykład, *"Elementy"* Euklidesa i&nbsp;*"Zasady psychologii"* W. Jamesa to dwie podobne książki (obie naukowe), ale z&nbsp;różnych dziedzin naukowych. Podobnie, *"Polityka"* Arystotelesa i&nbsp;*"Bogactwo Narodów"* A. Smitha, choć dotyczą różnych tematów, można wskazać podobieństwa i&nbsp;różnice, jeśli masz pojęcie o *problemach praktycznych* i&nbsp;ich różnych rodzajach.

Podstawowa klasyfikacja obejmuje literaturę piękną i&nbsp;literaturę non-fiction (historia, filozofia, matematyka, nauki ścisłe, nauki społeczne).
Inny podział to książki teoretyczne i&nbsp;praktyczne.

**2. Opisz treść książki w&nbsp;jak największym skrócie.**

**3. Wylicz główne części książki w&nbsp;odpowiedniej kolejności i&nbsp;relacji oraz opisz je tak, jak opisałeś całość.**
Książka składa się z&nbsp;części, podobnie jak dom z&nbsp;cegieł i&nbsp;pokoi. W&nbsp;doskonale zbudowanej książce wszystko jest umiejscowione w&nbsp;odpowiednich miejscach, podobnie jak w&nbsp;doskonale zaprojektowanym domu. W&nbsp;przypadku złej struktury książki może to być równie frustrujące, jak w&nbsp;przypadku źle zaprojektowanego domu, gdzie pokoje są źle rozmieszczone.
Każda część książki może mieć swoją niezależną strukturę, ale musi być powiązana z&nbsp;innymi częściami, aby wnosić coś do całości książki.

Im lepsza struktura książki, tym łatwiej ją zrozumieć.

Czasem możemy użyć innej struktury niż ta zaproponowana przez autora, jeśli uważamy, że jest bardziej odpowiednia.

**4. Zidentyfikuj problem lub problemy, które autor próbuje rozwiązać.**
Autor może (ale nie musi) wyraźnie formułować pytania, które go interesują.
Ważne jest, aby określić główne problemy, na które autor próbuje odpowiedzieć.

**5. Zrozum kluczowe terminy i&nbsp;pojęcia autora.**
Język nie zawsze jest precyzyjny, dlatego czytelnik i&nbsp;autor muszą mówić *tym samym językiem*.

**6. Wydobądź główne tezy autora, starając się zrozumieć kluczowe zdania i&nbsp;twierdzenia.**
Podczas analizy staraj się najpierw odnaleźć najważniejsze słowa, a&nbsp;następnie najważniejsze zdania i&nbsp;tezy oraz argumenty autora.

Gdy rozumiesz główne zdania, musisz zrozumieć, co reprezentują te tezy. Aby to osiągnąć, powinieneś być w&nbsp;stanie wyrazić je innymi słowami. Jeśli tego nie potrafisz, oznacza to, że nie rozumiesz ich, zostały ci przekazane jedynie słowa, a&nbsp;nie wiedza.
Kolejnym testem jest próba przedstawienia przykładu lub doświadczenia, które pasuje do opisanej tezy. Musisz być w&nbsp;stanie skonstruować przykład, który ilustruje ogólną prawdę zawartą w&nbsp;zdaniu.

Tezy stanowią odpowiedzi na pytania, sformułowane są jako deklaracje wiedzy lub opinii.
Często wielu autorów prezentuje te same tezy, używając innych słów i&nbsp;zdań.

**7. Zrozum argumenty autora, identyfikując lub konstruując je z&nbsp;sekwencji zdań.**
Argumentacja składa się z&nbsp;ciągu tez, z&nbsp;których niektóre stanowią uzasadnienie dla innych.
Argumentacja może być zawarta w&nbsp;jednym zdaniu, kilku zdaniach, paragrafie lub rozciągać się na kilka paragrafów.

**8. Określ, które problemy autor rozwiązał, a&nbsp;które pozostały nierozstrzygnięte.** W&nbsp;przypadku tych ostatnich, rozważ, czy autor zdawał sobie sprawę z&nbsp;ich nierozwiązania.

**9. Nie zaczynaj krytyki, dopóki nie zakończysz analizy książki (nie mów, że się zgadzasz, nie zgadzasz się, ani nie zawieszaj oceny, dopóki nie zrozumiesz).**
Jeśli starasz się zrozumieć książkę, a&nbsp;mimo to nie jesteś w&nbsp;stanie, to problem z&nbsp;tekstem, nie z&nbsp;Tobą.
Zanim wydasz opinię, upewnij się, czy dane dzieło nie jest powiązane z&nbsp;innymi, które wpływają na jego zrozumienie. Na przykład nie można zrozumieć *"Manifestu komunistycznego"* Marksa bez uwzględnienia jego *"Kapitału"*.

**10. Jeśli nie zgadzasz się z&nbsp;autorem, nie zaczynaj konfrontacyjnej krytyki.**
Nie powinieneś czytać książki z&nbsp;założeniem wyszukania w&nbsp;niej błędów.
Celem czytania jest nauka, a&nbsp;nie wygrywanie w&nbsp;sporze.
Powinieneś być otwarty na to, by zarówno zgadzać się, jak i&nbsp;nie zgadzać się z&nbsp;autorem.

Jeśli nie rozumiesz tekstu i&nbsp;wina leży po stronie tekstu, musisz wskazać konkretne fragmenty gdzie: struktura jest nieuporządkowana, części nie są ze sobą powiązane, słowa/terminy nie są używane przez autora systematycznie, przez co doprowadza to do nieporozumień.

Jeśli rozumiesz tekst i&nbsp;zgadzasz się z&nbsp;nim, nie ma więcej pracy. W&nbsp;przeciwnym razie trzeba zidentyfikować błędy autora.

**11. Pokaż różnicę między wiedzą a&nbsp;osobistą opinią, prezentując uzasadnienie każdej krytycznej oceny.**
Wiedza jest opinią, którą można uzasadnić.
Różnice zdań wynikają z&nbsp;braku zrozumienia (język jest niedoskonały), różnic w&nbsp;poziomie wiedzy i&nbsp;sposobach myślenia. 
Te różnice można zniwelować, osiągając porozumienie dzięki nauce.

**12. Wskaż obszary, w&nbsp;których autor jest niewystarczająco poinformowany.**
Autor jest niewystarczająco poinformowany, jeśli brakuje mu istotnych informacji lub wiedzy dotyczącej danego problemu. Trzeba określić, czego autorowi brakuje i&nbsp;dlaczego to jest istotne.

**13. Wskaż obszary, w&nbsp;których autor jest źle poinformowany.**
Autor jest źle poinformowany, jeśli twierdzi, że pewne warunki są spełnione, choć w&nbsp;rzeczywistości nie są. Autor twierdzi, że posiada wiedzę, którą nie ma. Taki błąd trzeba ujawnić i&nbsp;pokazać, że przeciwna opinia jest bardziej prawdziwa i&nbsp;wiarygodna.

**14. Wskaż obszary, w&nbsp;których autor jest nielogiczny.**
Autor jest nielogiczny, jeśli jego rozumowanie zawiera błędy.
* Błąd *non sequitur* oznacza *nie wynika to*, czyli wniosek nie wynika logicznie z&nbsp;przesłanek.
* Błąd niespójności dowodu oznacza, że dwie rzeczy, które autor próbuje powiedzieć, są nie do pogodzenia (istnieje sprzeczność w&nbsp;ich wnioskach).

**15. Wskaż obszary, w&nbsp;których analiza autora jest niekompletna.**
Analiza jest niekompletna, jeśli autor nie rozwiązał wszystkich problemów, nie wykorzystał pełni materiału, nie uwzględnił wszystkich implikacji i&nbsp;konsekwencji, lub nie zastanowił się nad wszystkimi możliwościami.
Brak kompletności powinno się wykazać, wskazując na luki za pomocą własnej wiedzy lub innych źródeł.

## Wskazówki do czytania analitycznego

### Książki praktyczne vs. teoretyczne

#### Książki praktyczne

* Skupiają się na rzeczach, które działają krótko- i&nbsp;długoterminowo.
* Inspirują do działania i&nbsp;wykonywania konkretnych czynności.
* Przekazują wiedzę, ale ich nacisk jest na praktycznych zastosowaniach do rozwiązywania problemów.
* Wskazują, jak coś zrobić lub wykonywać określone czynności.

Rodzaje książek praktycznych:

* Książki inżynieryjne.
* Książki medyczne.
* Książki kulinarne.
* Książki o&nbsp;moralności, zasadach i&nbsp;etyce -  uczą, jak żyć i&nbsp;postępować zgodnie z&nbsp;określonymi wartościami.
* Książki o&nbsp;problemach politycznych - doradzają, co robić i&nbsp;jak reagować na konkretne kwestie.
* Książki ekonomiczne (poza reporterską, matematyczną czy statystyczną analizą zachowań ekonomicznych) - pomagają w&nbsp;organizacji życia ekonomicznego, zarówno na poziomie indywidualnym, jak i&nbsp;społecznym, i&nbsp;wskazują, co robić i&nbsp;jakie koszty ponosić.

Książki praktyczne można podzielić na dwie kategorie:

* Książki prezentujące konkretne zasady, na przykład książki kucharskie lub poradniki.
* Książki, które przedstawiają ogólne reguły generujące zasady, na przykład książki ekonomiczne, polityczne lub o&nbsp;moralności.

Podczas czytania książki praktycznej warto zastanowić się nad kilkoma kwestiami:

* Jaki jest cel autora?
* Jakie środki proponuje, aby osiągnąć ten cel?

Książki praktyczne często zawierają elementy retoryki i&nbsp;propagandy, gdzie autor stara się przekonać czytelnika do swoich racji. 
Dlatego osobowość autora może mieć większe znaczenie w&nbsp;tego rodzaju książkach niż w&nbsp;książkach teoretycznych.

Jeśli po przeczytaniu książki praktycznej zgadzasz się z&nbsp;jej treścią, nie ma przeszkód, aby wdrożyć proponowane zasady w&nbsp;życie.

#### Książki teoretyczne

* Skupiają się na rzeczach, które trzeba zrozumieć.
* Przekazują wiedzę.
* Prezentują, że dana rzecz jest faktem..
* Starają się odpowiedzieć na pytania, czy coś jest prawdą, słuszne lub zasadne.

Rodzaje książek teoretycznych:

* Książki historyczne - opisują wydarzenia z&nbsp;przeszłości i&nbsp;skupiają się na narracji.
* Książki naukowe - zajmują się abstrakcyjnymi teoriami i&nbsp;ogólnymi zasadami, niekoniecznie konkretnymi wydarzeniami z&nbsp;przeszłości.
* Książki filozoficzne - starają się odkryć ogólne zasady i&nbsp;głębsze sensy.

**Jak odróżnić filozofię od nauk ścisłych?**
Jeśli książka teoretyczna kładzie nacisk na zagadnienia wykraczające poza codzienne doświadczenia, opiera się na eksperymentach, obserwacjach i&nbsp;dowodach empirycznych, to zaliczamy ją do nauk ścisłych.
Przykłady takich książek to *"Optyka"* Newtona, który opierał się na eksperymentach z&nbsp;pryzmatami, lustrami i&nbsp;światłem, lub *"O&nbsp;powstaniu gatunków"* Darwina, opisujący wieloletnie obserwacje w&nbsp;przyrodzie.

Książki filozoficzne nie odwołują się do faktów lub obserwacji, które leżą poza doświadczeniami zwykłego człowieka.

### Odnajdywanie istotnych słów i&nbsp;zdań

Słowo, w&nbsp;jednym konkretnym znaczeniu, definiuje termin. Termin może być opisany przez wiele słów.

> Na przykład, słowo X w&nbsp;znaczeniach a, b, c definiuje terminy Xa, Xb, Xc.
> Słowo Y w&nbsp;znaczeniu d (termin Yd) może równoważnie opisywać termin Xa, co oznacza, że Xa = Yd.

Zasady identyfikacji kluczowych słów:

* Słowo staje się ważne, jeśli sprawia trudność w&nbsp;zrozumieniu.
* Słowa ci *obce*.
* Słowa zdefiniowane przez autora, zwane słownictwem branżowym (każda dziedzina nauki ma własne słownictwo), a&nbsp;filozofowie często używają unikalnego słownictwa.
* Słowa podkreślone przez autora.
* Słowa, wokół których autor toczy spór lub dyskusję z&nbsp;innymi.

Aby zrozumieć znaczenie nieznanego słowa, należy uwzględnić kontekst i&nbsp;skorzystać z&nbsp;definicji innych słów.

Podobnie jak w&nbsp;przypadku słów i&nbsp;terminów, zdania i&nbsp;tezy nie mają relacji 1:1. 
Oznacza to, że ta sama teza może być wyrażona różnymi słowami czy zdaniem.

Rozpoznawanie najważniejszych zdań:
* Zdania trudne w&nbsp;interpretacji są ważne z&nbsp;perspektywy czytelnika.
* Zdania wyrażające poglądy i&nbsp;opinie, na których opiera się argumentacja autora, są ważne z&nbsp;punktu widzenia samego autora.
* Autor może celowo wyróżnić ważne zdania.
* Kluczowe słowa wskazują często na ważne zdania.

### Non-fiction
Historia to opowieść o&nbsp;przeszłości, a&nbsp;historyk nie powinien zmyślać faktów. Jednak pamiętajmy, że każda historia jest zawsze przedstawiana z&nbsp;określonego punktu widzenia.

Kiedy czytasz stare książki naukowe, skup się na zrozumieniu historii i&nbsp;filozofii nauki (do zdobycia aktualnej wiedzy lepiej sięgnąć po teksty współczesne).

Przy czytaniu książek naukowych, zwłaszcza jako laik, twoim głównym celem powinno być zrozumienie problemu, a&nbsp;nie zdobywanie specjalistycznych umiejętności w&nbsp;danej dziedzinie. Dlatego warto sięgnąć po dobre książki popularnonaukowe:
* Ograniczają ilość skomplikowanych opisów eksperymentów.
* Unikają nadmiaru trudnej matematyki.

### Beletrystyka

Nie oceniaj literatury pięknej, dopóki nie zrozumiesz w&nbsp;pełni, co autor pragnął, abyś doświadczył.
Twoja krytyka tekstu będzie bardziej odzwierciedleniem twojej osobistej perspektywy, upodobań itp. niż obiektywną oceną.

#### Literatura Piękna

Literatura piękna stara się przekazywać uczucia i/lub doświadczenia, a&nbsp;nie fakty czy wiedzę.
Jej celem jest oddziaływanie na wyobraźnię czytelnika.
Skupia się na tym, co nie zostało powiedziane wprost, a&nbsp;ukryte między wierszami, wykorzystując metafory itp. 
Odróżnia się tym od książek naukowych, które dążą do maksymalnej precyzji.
W&nbsp;literaturze pięknej nie poszukujemy terminów, tez, czy argumentacji.

Z&nbsp;opowieści można również czerpać naukę, podobnie jak z&nbsp;własnych doświadczeń życiowych.
Czytając opowieść, oczekujemy historii, która *mogłaby się wydarzyć* w&nbsp;świecie bohaterów i&nbsp;wydarzeń opisanych przez autora.
Wszystkie elementy fikcji tworzą świat przedstawiony, w&nbsp;którym bohaterowie *żyją, poruszają się i&nbsp;mają swoje jestestwo*.
Poprzez manipulację elementami autor opowiada historię.

Fabuła musi wpasować się w&nbsp;ustalony schemat czasowy: początek, rozwinięcie, zakończenie.
Aby zrozumieć fabułę, musisz zidentyfikować jej punkt początkowy (niekoniecznie związany z&nbsp;początkiem książki), śledzić jej rozwój, poznać punkt kulminacyjny, zrozumieć, jakie działania do niego doprowadziły, i&nbsp;dowiedzieć się, co się dzieje po nim. 
Fabuła jest *duszą opowieści*.

Pełnego zrozumienia historii nie osiągniesz, jeśli nie zapoznasz się dokładnie z&nbsp;bohaterami. 
Dlatego podczas czytania opowieści musisz:

* Wczuć się w&nbsp;świat przedstawiony.
* Poznać go tak, jakbyś był obserwatorem wydarzeń.
* Stać się członkiem tego świata, gotowym zaprzyjaźnić się lub zaznajomić się z&nbsp;bohaterami; uczestniczyć w&nbsp;wydarzeniach poprzez życzliwe zrozumienie, tak jakbyś przeżywał je razem z&nbsp;przyjacielem.

Jeśli to zrobisz, przestaniesz postrzegać bohaterów jako odizolowane figury na szachownicy, a&nbsp;zaczniesz dostrzegać powiązania, które nadają im życie i&nbsp;czynią z&nbsp;nich członków społeczeństwa.

**Zasada czytania powieści - czytaj szybko i&nbsp;z całkowitą imersją.**

#### Dramat

Tekst dramatu nie stanowi pełnego dzieła, gdyż dramat jest przeznaczony do bycia oglądanym na scenie. 
Dlatego, aby zrozumieć dramat, trzeba go *wyreżyserować* w&nbsp;umyśle.

W&nbsp;historii przedstawianej w&nbsp;tragedii greckiej zawsze brakuje czasu. 
Czytając, możemy rozważać inne rozwiązania problemów niż te, które proponują bohaterowie. 
Pytanie brzmi, czy sami wpadlibyśmy na nie, biorąc pod uwagę okoliczności, w&nbsp;jakich znaleźli się bohaterowie.

W&nbsp;teatrze greckim główni aktorzy nosili specjalne buty (koturny), co sprawiało, że byli wyżsi - warto pamiętać o&nbsp;tym podczas lektury.

## Czytanie synoptyczne

Dotyczy czytania wielu książek na dany temat lub problem, gdy chcemy go zgłębić.

### Zasady czytania synoptycznego

**1. Stwórz bibliografię dotyczącą problemu**
* Opracuj wstępną bibliografię, korzystając z&nbsp;katalogów bibliotecznych, bibliografii w&nbsp;książkach, itp.
* Przeszukaj wszystkie książki z&nbsp;wstępnej bibliografii, aby zweryfikować, które są istotne dla twojego problemu i&nbsp;uzyskać bardziej precyzyjne pojęcie na temat omawianego zagadnienia.

> Uwaga: Te dwa etapy nie są ściśle mówiąc chronologicznie różne; mają one na siebie wpływ, zwłaszcza drugi służy do modyfikacji pierwszego.

**2. Przejrzyj książki, które zostały już zidentyfikowane jako istotne w&nbsp;punkcie 1, aby znaleźć najbardziej odpowiednie fragmenty.**

**3. Doprowadź autorów do porozumienia, konstruując neutralną terminologię**
Różni autorzy mogą używać różnych terminów do opisania problemu.
Musisz stworzyć terminy (neutralny język) i&nbsp;dostosować autorów do nich, a&nbsp;nie odwrotnie. 
Innymi słowy, musisz skłonić autora do używania twojego języka, a&nbsp;nie swojego.

Jest to jedno z&nbsp;trudniejszych zadań.
Czytanie synoptyczne to duże wyzwanie w&nbsp;zakresie tłumaczenia.

**4. Ustal zestaw neutralnych tez dla wszystkich autorów, formułując zestaw pytań, na które wszyscy lub większość autorów może odpowiedzieć, niezależnie od tego, czy faktycznie rozważają te pytania wprost, czy nie.**
Autorzy starają się odpowiedzieć na własne pytania.
Ponieważ tworzymy własne słownictwo i&nbsp;dopasowujemy do niego autorów, powinniśmy również stworzyć pytania, na które szukamy odpowiedzi w&nbsp;tekstach.
Nasze pytania niekoniecznie muszą pokrywać się z&nbsp;pytaniami autora.
Pytania muszą być ułożone w&nbsp;taki sposób, który ułatwi nam rozwiązanie naszego problemu.

**5. Zdefiniuj konflikty (między autorami), zarówno główne, jak i&nbsp;drugorzędne, poprzez uporządkowanie przeciwnych odpowiedzi autorów na różne pytania po jednej lub drugiej stronie zagadnienia.** 
Należy pamiętać, że konflikt nie zawsze jest wyraźny między autorami lub między nimi, ale czasami musi być skonstruowany poprzez interpretację poglądów autorów w&nbsp;sprawach, które mogły nie być ich głównym zmartwieniem.

**6. Analizuj dyskusję, porządkując pytania i&nbsp;kwestie w&nbsp;taki sposób, aby rzucić jak najwięcej światła na temat.** 
Bardziej ogólne zagadnienia powinny poprzedzać mniej ogólne, a&nbsp;relacje między zagadnieniami powinny być jasno wskazane.

## Bibliografia
\[Adler and van Doren\] M.J. Adler and C. van Doren. *How to read a&nbsp;book. The classic guide to intelligent reading.* Touchstone, New York, 2014. [Link do książki w&nbsp;amazon.pl](https://www.amazon.pl/How-Read-Book-Classic-Intelligent/dp/0671212095)
