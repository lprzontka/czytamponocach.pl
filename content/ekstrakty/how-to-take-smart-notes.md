---
title: Sönke Ahrens - How to take smart notes
showToc: true
date: 2024-12-31T12:30:00
aliases:
 - howtotakesmartnotes
---

Jest to praktyczny poradnik dotyczący systemu Zettelkasten.
Książka jest skierowana do studentów, naukowców i&nbsp;pisarzy non-fiction – osób, które nie tylko tworzą artykuły, eseje czy książki, ale także prowadzą codzienne zapiski, aby utrwalać idee, cytaty itp.

Kiedy mówimy o&nbsp;pisaniu, zazwyczaj skupiamy się na długich formach, rzadko zwracając uwagę na codzienne notowanie, które paradoksalnie zajmuje najwięcej czasu.
Pismo to medium, w&nbsp;którym dokonuje się proces myślowy – badania i&nbsp;nauka często zachodzą właśnie w&nbsp;trakcie tworzenia tekstu.
Książki o&nbsp;pisaniu zazwyczaj należą do jednej z&nbsp;dwóch kategorii:
* *Formalne* – omawiające styl, strukturę tekstu, sposób cytowania itp.
* *Psychologiczne* – dotyczące podejścia do samego procesu pisania.

Obie te kategorie pomijają istotną część pisania, jaką jest robienie notatek.
*How to take smart notes* wypełnia tę lukę, przedstawiając system, który pozwala efektywnie sporządzać notatki, pracować z&nbsp;tekstami, rozwijać idee, a&nbsp;także ułatwia tworzenie własnych tekstów.

Treść książki można podzielić na trzy główne części:
* *Wprowadzenie* – ogólne omówienie systemu i&nbsp;jego historii.
* *Robienie notatek* – zasady i&nbsp;reguły systemu Zettelkasten dotyczące notowania.
* *Pisanie tekstów* – wskazówki, jak korzystać z&nbsp;systemu i&nbsp;notatek, aby na ich podstawie tworzyć teksty.

## Robienie notatek
Cały system notowania i&nbsp;pracy opiera się na metodzie stosowanej przez Niklasa Luhmanna – niemieckiego profesora socjologii.
Korzystał on z&nbsp;systemu Zettelkasten (*"pudełko na notatki"*, Slip-box), który pełnił funkcję zewnętrznego narzędzia wspierającego proces myślenia (*"external system to think in"*).

Trzymanie notatek w&nbsp;kilku różnych miejscach jest nieefektywne – wymaga choćby zapamiętania, gdzie co się znajduje.
Jeśli notatki kategoryzujemy tematycznie, musimy zdecydować, gdzie dana notatka najlepiej pasuje (np. do którego folderu).
W&nbsp;standardowych systemach notowania notatka może znajdować się tylko w&nbsp;jednym miejscu – folderze, zeszycie czy kontekście.
W&nbsp;systemie Zettlekasten wszystkie notatki lądują do Slip-boxa.

### Trzy typy notatek
* **Tymczasowe** – służą do szybkiego zapisywania pomysłów, gdy zajmujemy się czymś innym.
Im więcej czasu upływa od ich zapisania do opracowania, tym mniej są przydatne – zapominamy bowiem, dlaczego je zapisaliśmy i&nbsp;co miały oznaczać.
* **Permanentne** – pisane w&nbsp;taki sposób, by były zrozumiałe nawet po wielu latach:
  * Każda z&nbsp;nich ma potencjał, by stać się częścią większego tekstu lub zainspirować powstanie nowego.
  * Nie są jedynie przypomnieniami o&nbsp;ideach i&nbsp;przemyśleniach – same zawierają idee i&nbsp;przemyślenia w&nbsp;pełnej, zapisanej formie.
* **Indeksy** – odnoszą się do specyficznych projektów lub tematów, ułatwiając organizację i&nbsp;wyszukiwanie związanych z&nbsp;nimi notatek.

### Potrzebne narzędzia
* **Coś do pisania i&nbsp;miejsce do notowania** – może to być długopis i&nbsp;notatnik, laptop lub inne urządzenie do zapisywania.
* **System do zarządzania bibliografią** – na przykład program Zotero, który ułatwia organizację i&nbsp;przechowywanie źródeł.
* **Slip-box** – system do notowania, który można prowadzić w&nbsp;formie papierowej lub elektronicznej.
* **Edytor tekstu** – do opracowywania tekstów na późniejszych etapach pracy.

> "Tools are only as good as your ability to work with them." [Ahrens, s.31]

### Zasady notowania
* **Powiązanie z&nbsp;istniejącymi notatkami** - Notatki piszemy, mając na uwadze już istniejące wpisy.
Dzięki temu uwzględniamy szerszy kontekst i&nbsp;tworzymy sieć powiązań, zamiast skupiać się wyłącznie na pojedynczej informacji.
* **Przekładanie idei na własny język** - Idee, które chcemy zanotować, są zawarte w&nbsp;tekście w&nbsp;konkretnym kontekście, wspierają argument lub są częścią teorii opisanej innym językiem (terminologią, stylem).
Dlatego należy je przetłumaczyć na swój język i&nbsp;dostosować do kontekstów w&nbsp;Slip-boxie.
* **Autonomia notatek** - Notatki piszemy tak, jakby miały być zrozumiałe dla kogoś innego.
Oznacza to, że muszą być autonomiczne: zapisane pełnymi zdaniami, z&nbsp;podaniem źródeł i&nbsp;wszystkimi niezbędnymi informacjami.
* **Jedna notatka na jedną ideę** - Każda notatka powinna zawierać tylko jedną, jasno określoną ideę.
Dzięki temu łatwiej zarządzać treścią i&nbsp;łączyć ją z&nbsp;innymi notatkami.

### Dodawanie notatki do systemu
1. Dodaj nową notatkę bezpośrednio za tą, do której się odwołujesz.
Jeśli tworzysz notatkę, która nie kontynuuje konkretnej idei już istniejącej w&nbsp;systemie, umieść ją za ostatnią notatką w&nbsp;systemie.
2. Połącz nową notatkę z&nbsp;innymi, dodając do niej referencje do istniejących notatek lub tworząc linki w&nbsp;przeciwnym kierunku, tj. z&nbsp;innych notatek do nowej.
3. Upewnij się, że notatka będzie możliwa do znalezienia w&nbsp;przyszłości.
Powinna być powiązana referencjami z&nbsp;innymi notatkami lub wpisana w&nbsp;indeks.
4. Staraj się rozwijać sieć uogólnionych pomysłów, faktów i&nbsp;modeli mentalnych, która umożliwia głębsze zrozumienie i&nbsp;odkrywanie nowych powiązań.

### Najczęstsze błędy w&nbsp;notowaniu
* Traktowanie wszystkich notatek jako permanentnych i&nbsp;zapisywanie ich w&nbsp;notatnikach.
  * Chronologiczne kategoryzowanie utrudnia późniejszą pracę z&nbsp;notatkami.
  * Niektóre idee i&nbsp;pomysły, po głębszym zastanowieniu, okazują się mniej wartościowe lub nieprzydatne.
* Zbieranie notatek związanych z&nbsp;danym projektem w&nbsp;nowym miejscu.
  * Każdy nowy projekt wymaga zaczynania pracy od zera, co jest nieefektywne.
  * Gdy każda nowa idea prowadzi do rozpoczęcia nowego projektu, łatwo doprowadzić do sytuacji, w&nbsp;której mamy wiele nierozwiązanych i&nbsp;porzuconych projektów.
* Traktowanie wszystkich notatek jako tymczasowych.
  * Brak odpowiedniego opracowania sprawia, że notatki szybko tracą na wartości i&nbsp;stają się bezużyteczne.

### Slip-box (Zettelkasten)
Slip-box nie powinien być używany jako archiwum notatek ani jako *"cmentarz idei"*, nie jest również encyklopedią.
Jego wartość rośnie wraz z&nbsp;jego rozmiarami – im więcej notatek, tym bardziej staje się przydatny.
To jednocześnie jego wada, ponieważ skuteczna praca wymaga dużej liczby notatek.
Slip-box jest jednak przede wszystkim generatorem idei, który rozwija się równolegle z&nbsp;jego użytkownikiem.
Praca z&nbsp;Slip-boxem polega mniej na wyszukiwaniu konkretnych notatek, a&nbsp;bardziej na odkrywaniu powiązań między istotnymi faktami i&nbsp;pogłębianiu zrozumienia poprzez łączenie idei.
System ten wymusza selektywne podejście do czytania i&nbsp;notowania – jedynym kryterium dodawania notatek jest odpowiedź na pytanie, czy dana informacja wnosi coś wartościowego do systemu.

Tworzenie notatek permanentnych przypomina bardziej proces myślenia na papierze i&nbsp;dialogu z&nbsp;istniejącymi notatkami, niż zwykłe zapisywanie gotowych pomysłów.
Pisanie notatek i&nbsp;ich organizowanie w&nbsp;Slip-boxie to nic innego jak próba zrozumienia szerszego kontekstu danego tematu.
System ten wymaga od nas rozwijania, analizowania i&nbsp;łączenia idei, co prowadzi do głębokiego uczenia się. 

Uczenie się polega na łączeniu informacji z&nbsp;jak największą liczbą znanych nam kontekstów – dokładnie to osiągamy, łącząc notatki w&nbsp;systemie między sobą.
W&nbsp;tym systemie połączenia między notatkami są ważniejsze niż odniesienia z&nbsp;indeksu do pojedynczej notatki.
Notatki są wartościowe w&nbsp;takim stopniu, w&nbsp;jakim są częścią sieci, w&nbsp;których zostały osadzone.

## Pisanie
Każda myśl o&nbsp;pewnym poziomie złożoności wymaga pisania.

Kiedy uświadomimy sobie, że pisanie jest kluczowe, zmienia się nasze podejście do nauki i&nbsp;pracy intelektualnej:
* Nauczysz się formułować lepsze argumenty.
* Argumentacja musi być dokładnie przemyślana, zanim zostanie zapisana.
* Sposób czytania ulegnie zmianie — będziesz bardziej skupiony na istotnych aspektach, ponieważ nie da się zapisać wszystkiego.
* Skupienie na znaczeniu ułatwia zapamiętywanie idei.
* Myślenie w&nbsp;szerszym kontekście niż sam czytany tekst pozwala przekształcić idee w&nbsp;coś nowego.

Większość poradników pisania proponuje standardowy plan, (cf. [How to Read a&nbsp;Book](/ekstrakty/how-to-read-a-book/), czytanie synoptyczne):
1. Zdecyduj, o&nbsp;czym pisać.
2. Przeprowadź badania.
3. Napisz tekst.

Jednak ten plan często prowadzi do problemów:
* Aby postawić dobre pytanie, na które warto odpowiedzieć, trzeba mieć już pewną wiedzę na dany temat.
* Aby zdobyć tę wiedzę, trzeba co nieco przeczytać.
* Aby zdecydować, co czytać, trzeba już mieć jakąś orientację w&nbsp;temacie.

*To tworzy błędne koło.* Plan ten jest wyidealizowany i&nbsp;nie uwzględnia rzeczywistych trudności.
Dlatego warto zapytać: dlaczego nie opracować bardziej realistycznego podejścia?
Odwrócenie tego procesu wydaje się bardziej logiczne i&nbsp;praktyczne.

### Praca z Slip-boxem i pisanie krok po kroku
1. Zawsze miej pod ręką coś, gdzie możesz szybko zapisać swoje pomysły.
Trzymaj je w&nbsp;jednym miejscu do dalszego opracowania.
2. Podczas czytania rób notatki dotyczące treści, zapisując to, czego nie chcesz zapomnieć.
Notatki powinny być krótkie i&nbsp;najlepiej zapisane własnymi słowami.
Bądź selektywny w&nbsp;wyborze cytatów, ograniczając się do tych najistotniejszych.
3. Przejrzyj notatki z&nbsp;kroków 1 i&nbsp;2, zastanawiając się, jak łączą się one z&nbsp;istniejącymi już notatkami w&nbsp;systemie.
Zastanów się, co jest istotne dla twoich badań, przemyśleń czy zainteresowań.
Nie tylko zapisuj informacje, ale rozwijaj idee, argumenty i&nbsp;dyskusje.
Sprawdź: Czy nowa informacja potwierdza, zaprzecza lub rozwija istniejące idee? Czy możesz połączyć różne pomysły w&nbsp;coś nowego? Jakie pytania powstały w&nbsp;wyniku tej analizy?
4. Wprowadź notatkę do Slip-boxa, pamiętając o&nbsp;odpowiednim połączeniu jej z&nbsp;innymi notatkami.
5. Analizuj swoje notatki, sprawdzaj, czego brakuje, i&nbsp;identyfikuj pojawiające się pytania.
Czytaj, aby zakwestionować, wzmocnić, rozwinąć lub zmienić argumentację.
Twórz nowe notatki i&nbsp;rozwijaj istniejące idee.
Regularnie przeglądaj system, obserwując, gdzie tworzą się łańcuchy powiązań i&nbsp;klastry tematów.
6. Po pewnym czasie twój system będzie na tyle rozbudowany, że idee staną się wystarczająco rozwinięte, aby na ich podstawie pisać.
Temat wyłoni się naturalnie z&nbsp;posiadanych notatek.
Ułóż notatki w&nbsp;logiczną strukturę. Jeśli zauważysz luki w&nbsp;treści, uzupełnij je.
7. Napisz pierwszy draft.
Nie kopiuj notatek bezpośrednio.
Stwórz spójny tekst, bazując na notatkach, ale formułując własne przemyślenia.
8. Dopracuj swój tekst, uwzględniając wszystkie zebrane informacje i&nbsp;rozwinięte argumenty.


## Bibliografia
\[Ahrens\] Sönke Ahrens. *How to take smart notes. One simple technique to boost writing, learning and
    thinking.* Sönke Ahrens, Hamburg, 2022. [Link do książki w&nbsp;amazon.pl](https://www.amazon.pl/How-Take-Smart-Notes-Technique/dp/3982438802/)
