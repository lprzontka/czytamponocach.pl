---
title: Ajschylos - Oresteja
date: 2024-05-26T12:30:00
---

Po wcześniejszej lekturze [Prometeusza Skowanego](/klasyki/ajschylos-prometeusz-skowany/) podchodziłem do _Orestei_ Ajschylosa z&nbsp;pewnym sceptycyzmem.
Obawiałem się ciężkiego, monotonnego tekstu, ale miło się zaskoczyłem.
To naprawdę dynamiczne i&nbsp;wciągające dzieło, w&nbsp;którym sporo się dzieje – pełne intryg, zemsty i&nbsp;moralnych dylematów.
Tragedia ukazuje nie tylko brutalność starożytnych losów, ale też rodzący się porządek sprawiedliwości.
Zdecydowanie warto przeczytać!

## O&nbsp;treści
W&nbsp;skład _Oresteji_ wchodzą _Agamemnon_, _Ofiarnice_ i&nbsp;_Eumenidy_.

### W&nbsp;jednym zdaniu
_Oresteja_ opowiada o&nbsp;zemście Klitajmestry na Agamemnonie oraz jej konsekwencji na Orestesie.

### Zarys fabuły
 Agamemnon w&nbsp;trakcie wyprawy na Troję złożył swoją córkę Ifigenię w&nbsp;ofierze bogini Artemidy.
Po powrocie do domu z&nbsp;wojny został on zamordowany w&nbsp;akcie zemsty przez Klitajmestrę.
Wraz z&nbsp;nim zginęła również jego branka Kasandra (córka Pryjama).
Do spisku przeciwko Agamemnonowi przyczynił się również Ajgistos, który wraz z&nbsp;Klitajmestrą zaczął rządzić po śmierci króla.


Klitajmestra wysłała Orestesa do Strofiosa na wychowanie z&nbsp;obawy przed buntem, gdyby Agamemnon zginął pod Troją.
Apollo rozkazał Orestesowi pomścić śmierć swego ojca, więc Orestes w&nbsp;ukryciu wrócił do domu.
Wraz z&nbsp;poparciem Elektry (siostry) zabił Ajgistosa i&nbsp;swoją matkę.
Jednakże czyn ten sprawdził na niego Erynie (boginie zemsty i&nbsp;wyrzutów sumienia), które zaczęły go prześladować.
Orestes udał się do Aten, gdzie podczas procesu został oczyszczony z&nbsp;winy.

### Główne zagadnienia
Jakie są główne tematy opowieści, o&nbsp;czym ona jest? Np, o&nbsp;miłości, o&nbsp;bohaterstwie.

#### Zemsta
Głównym motywem trylogii jest zemsta, która stanowi motywację dla zachowań bohaterów.
Czyn Klitajmestry został potępiony przez chór.
Jednakże zabójstwo córki przez Agamemnona nie wywołało sprzeciwu.
Klitajmestra zarzuca cnotliwemu chórowi hipokryzję.

> Skazujesz mnie tej chwili na sromne wygnanie.  
> Pogardą ludu grozisz! Niewczesne łajanie!  
> Bo powiedz, czemuś wówczas nie obwiniał męża,  
> Gdy powziął myśl haniebną, że miłość zwycięża  
> Ojcowską, i, ni jagnię z&nbsp;swojej licznej trzody,  
> Poświęcił własną córkę, mego łona młody,  
> Uroczy kwiat, by trackie uspokoić burze?  
> Dlaczegoście go wówczas, srodzy prawa stróże,  
> Za zbrodni tych ohydę, nędznego mordercę,  
> Z&nbsp;tej ziemi nie wygnali, gdy tak moje serce  
> Skwapliwie dziś karzecie?

Orestes mści się za zabójstwo ojca, ale nie podjął żadnych działań, aby pomścić zamordowaną siostrę.

#### Sprawiedliwość
Zarówno Klitajmestra, jak i&nbsp;Orestes, byli przekonani o&nbsp;sprawiedliwości swoich czynów.
Obydwoje poddali się emocjom i&nbsp;próbowali wymierzyć sprawiedliwość poprzez zemstę i&nbsp;morderstwo.
Ten cykl został przerwany przez Atenę w&nbsp;_Eumenidach_.
Zamiast dopuścić do kolejnej zemsty, postanowiła ona na zimno przeprowadzić osąd nad czynem Orestesa.
Wysłuchała obu stron (Erynii oraz Orestesa) i&nbsp;poddała ocenie ławie przysięgłych.
W&nbsp;ten sposób powstał fundament prawa.

## Bibliografia
\[Ajschylos\] Ajschylos.
*Oresteja*.
Fundacja Nowoczesna Polska, 2022.
[Link do książki w&nbsp;WolneLektury.pl](https://wolnelektury.pl/katalog/lektura/ajschylos-oresteja/)
