---
title: Ajschylos - Prometeusz skowany
date: 2024-02-25T12:30:00
---

Niestety, _Prometeusz Skowany_ Ajschylosa nie przypadł mi do gustu.
Spodziewałem się dramatu pełnego napięcia i&nbsp;emocji, a&nbsp;zamiast tego otrzymałem statyczną opowieść, w&nbsp;której niewiele się dzieje.
Cała akcja sprowadza się głównie do rozmów Prometeusza z&nbsp;kolejnymi postaciami, bez większego rozwoju fabularnego.
Choć sam mit jest fascynujący, jego realizacja w&nbsp;tym dramacie mnie rozczarowała.

## O&nbsp;treści
### W&nbsp;jednym zdaniu
_Prometeusz Skowany_ przedstawia wycinek życia Prometeusza, dokładniej dzień, w&nbsp;którym zostaje przykuty do skały i&nbsp;krótkie chwile po tym.

### Zarys fabuły
Prometeusz zostaje przykuty do skały przez Kratosa i&nbsp;Hefajstosa na rozkaz Zeusa.
W&nbsp;trakcie sztuki rozmawia z&nbsp;różnymi postaciami – Okeanosem, jego córkami Okeanidami oraz Io, śmiertelniczką prześladowaną przez Herę.
Aby nie narazić innych na gniew Zeusa, odmawia pomocy zarówno Okeanosowi, jak i&nbsp;Io.
Mimo własnego cierpienia przepowiada Io jej przyszły los.

### Główne zagadnienia
#### Przeznaczenie
Prometeusz, rozmawiając z&nbsp;Okeanidami, stwierdza, że przed przeznaczeniem nie ma ucieczki – nawet Zeus będzie musiał się mu podporządkować:

> PRZODOWNICA&nbsp;CHÓRU&nbsp; 
> A&nbsp;sterem Konieczności któż włada jedynie? 
>   
> PROMETEUSZ&nbsp; 
> Mojr trójca i&nbsp;pamiętne wszelkich win Erynie.
>    
> PRZODOWNICA&nbsp;CHÓRU&nbsp; 
> Od mocy ich czyż lichsza Zeusowa potęga?  
>  
> PROMETEUSZ&nbsp; 
> Nie ujdzie Przeznaczeniu, co po niego sięga.

#### Duma i&nbsp;upór
Prometeusz jest dumny i&nbsp;uparty.
Dopóki nie zgodzi się pomóc Zeusowi, pozostanie uwięziony.
Woli cierpieć niewyobrażalne katusze, niż się podporządkować.
Jest także zadufany w&nbsp;sobie, przekonany o&nbsp;swojej niezastąpionej roli.
Uważa, że:
* Zeus nie poradzi sobie bez jego rad,
* zwycięstwo w&nbsp;Tytanomachii było możliwe tylko dzięki niemu,
* to on ugruntował władzę Zeusa,
* jedynie on może ocalić Zeusa przed klęską, którą sprowadzą na niego własne miłosne igraszki.

## Bibliografia
\[Ajschylos\] Ajschylos.
*Prometeusz skowany*.
Fundacja Nowoczesna Polska, 2022.
[Link do książki w&nbsp;WolneLektury.pl](https://wolnelektury.pl/katalog/lektura/ajschylos-prometeusz-skowany/)
