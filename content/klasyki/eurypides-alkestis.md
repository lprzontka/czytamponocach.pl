---
title: Eurypides - Alkestis
date: 2024-07-28T12:30:00
---

_Alkestis_ to tragikomedia Eurypidesa opowiadająca historię Alkestis i&nbsp;Admeta, który miał umrzeć młodo. Jednak dzięki wstawiennictwu Apolla, dostaje on szansę na przedłużenie swojego życia, jeśli znajdzie kogoś, kto odda swoje życie w&nbsp;zamian. Jedyną osobą zdolną na takie poświęcenie jest jego żona, Alkestis. Wszystko kończy się szczęśliwie, gdy Herakles wydziera Alkestis z&nbsp;rąk Tanatosa i&nbsp;oddaje ją Admetowi.

## Miłość i&nbsp;poświęcenie
_Alkestis_ opowiada o&nbsp;miłości. Alkestis kochała swojego męża tak bardzo, że była w&nbsp;stanie oddać za niego swoje życie. Dokonała aktu, na który nikt inny nie był w&nbsp;stanie się odważyć. 

## Egoizm
Ważną częścią dramatu jest rozmowa Admeta z&nbsp;jego ojcem. Admet zarzuca mu, że nie był w&nbsp;stanie umrzeć za swoje dziecko.

> Podobnych, jak ty tchórzów, nie ma tu zbyt wielu:  
> Człek w&nbsp;latach, u&nbsp;żywota stojący już celu,  
> Odwagi ani woli-ś nie miał, by za dziecko  
> Poświęcić się!

Feres nie pozostaje mu dłużny i&nbsp;wytyka synowi hipokryzję, twierdząc, że tchórzem jest ten, kto pozwala kobiecie się poświęcić.

> Mnie tchórzem zwiesz, a&nbsp;powiedz, gdzie większy wyrasta  
> Tchórzliwiec ponad człeka, którego niewiasta  
> Zwycięża, za pięknego konając młodziana?

Feres dziwi się słowom Admeta, który sam kochał życie na ziemi tak bardzo, że pozwolił własnej żonie umrzeć za niego.

> Jakżeż hańbić można  
> Najbliższych, że nie czynią, co ci myśl ostrożna  
> Tak samo czynić wzbrania? Milcz! Sam kochasz ziemię  
> I&nbsp;dziwisz się, że w&nbsp;innych myśl ta sama drzemie.

## Bibliografia
\[Eurypides\] Eurypides. *Alkestis*. Akademia Umiejętności, 1918. [Link do książki w&nbsp;Wikiźródła](https://pl.wikisource.org/wiki/Alkestis_%28Eurypides%2C_t%C5%82um._Kasprowicz%2C_1918%29)
