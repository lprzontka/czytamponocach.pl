---
title: Eurypides - Hippolytos uwieńczony
date: 2025-01-26T12:30:00
---

_Hippolytos uwieńczony_ to tragedia Eurypidesa, która opowiada o&nbsp;losie Hippolytosa, syna Tezeusza. Hippolytos naraża się Afrodycie, gardząc jej kultem i&nbsp;poświęcając się wyłącznie Artemidzie. W&nbsp;ramach zemsty bogini miłości sprawia, że Fedra, żona Tezeusza, zakochuje się w&nbsp;Hippolytosie. Gdy ten odrzuca jej zaloty, Fedra w&nbsp;desperacji popełnia samobójstwo, pozostawiając list, w&nbsp;którym oskarża Hippolytosa o&nbsp;próbę uwiedzenia.

Tezeusz, pełen gniewu i&nbsp;żalu, przeklina swojego syna. Wkrótce Hippolytos ulega tragicznemu wypadkowi na rydwanie. Umierającego Hippolytosa sprowadzają do domu, gdzie Tezeusz poznaje prawdę o&nbsp;niewinności syna. Jednakże jest już za późno – Hippolytos umiera, wcześniej przebaczając ojcu.

## Pycha
Hippolytos, wierny wyznawca Artemidy, odrzuca Afrodytę, odmawiając oddania jej należnej czci i&nbsp;podporządkowania się jej woli. Afrodyta, urażona jego postawą, postanawia zemścić się na nim, rozkochując w&nbsp;nim Fedrę. 

Stary sługa, dostrzegając zagrożenie, ostrzega Hippolytosa przed ignorowaniem boskich sił, radząc mu, by nie przeciwstawiał się potędze Afrodyty:

> Daj bogom co boskiego, to ci, synu, radzę.

Pycha Hippolytosa i&nbsp;jego odmowa uznania potęgi Afrodyty prowadzą do zaostrzenia konfliktu między boginiami – Artemidą i&nbsp;Afrodytą. Rywalizacja boskich mocy wpływa nie tylko na życie Hippolytosa, ale również na losy Fedry i&nbsp;Tezeusza, doprowadzając do tragicznego finału. Ludzie, uwikłani w&nbsp;boskie spory, stają się bezsilnymi pionkami w&nbsp;rozgrywkach sił wyższych, niezdolni do wpłynięcia na wydarzenia, które ostatecznie niszczą ich życie.

## Gniew
Na początku dramatu Afrodyta zaprzecza, jakoby jej działaniami kierował gniew czy urażona duma:

> Zazdrości przecież we mnie zaszczyt ten nie budzi,  
Bo po cóż? [Afrodyta o&nbsp;zachowaniu Hippolyta]

Jednak pod koniec tragedii Artemida jasno stwierdza, że to właśnie gniew Afrodyty doprowadził do tragicznych wydarzeń:

> Kipryda to sprawiła, ażeby tak krwawo  
Nasycić swoje gniewy. 

Gniew nie ogranicza się jednak tylko do bogów. Tezeusz, zrozpaczony po odkryciu listu pozostawionego przez Fedrę, działa pod wpływem emocji. Nie szukając prawdy, przeklina swojego syna i&nbsp;doprowadza do jego zguby. Impulsywność Tezeusza okazuje się równie zgubna, jak gniew Afrodyty, prowadząc do śmierci Hippolytosa.

Dopiero interwencja Artemidy przynosi chwilowe ukojenie. Dzięki niej ojciec i&nbsp;syn godzą się w&nbsp;obliczu tragedii, a&nbsp;Hippolytos przebacza Tezeuszowi przed swoją śmiercią:

> Zdejmuję z&nbsp;ciebie, ojcze, krwawą mordu winę.

## Bibliografia
\[Eurypides\] Eurypides. *Hippolytos uwieńczony*. Fundacja Nowochesna Polska, 2023. [Link do książki w&nbsp;Wolne Lektury](https://wolnelektury.pl/katalog/lektura/eurypides-hippolytos-uwienczony/)
