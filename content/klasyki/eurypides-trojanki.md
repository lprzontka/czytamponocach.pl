---
title: Eurypides - Trojanki
date: 2024-06-30T12:30:00
---

Nie spodziewałem się, że _Trojanki_ Eurypidesa będą miały tak silny wydźwięk antywojenny.
Dramat w&nbsp;niezwykle poruszający sposób ukazuje cierpienie kobiet, które stały się ofiarami wojny.
Bardzo mi się podobało i&nbsp;polecam każdemu – to dzieło, które mimo upływu wieków pozostaje boleśnie aktualne.

## O&nbsp;treści
### W&nbsp;jednym zdaniu
Dramat przedstawia losy kobiet z&nbsp;Troi po tym, jak została ona zdobyta i&nbsp;splądrowana przez Greków.


### Zarys fabuły
Główną bohaterką jest Hekabe, była królowa i&nbsp;żona Pryjama.
Opłakuje ona poległych i&nbsp;użala się nad swoim losem przyszłej niewolnicy.
Z&nbsp;Andromechą, swoją synową, dyskutuje o&nbsp;tym, czy lepiej jest żyć jako niewolnica, czy też lepiej umrzeć.
Jakby tego było mało, to malutki syn Andromechy zostaje skazany na śmierć poprzez zrzucenie z&nbsp;murów! Pod koniec tragedii Hekabe musi pochować swego wnuka i&nbsp;patrzeć na równanie Troi z&nbsp;ziemią.

### Główne zagadnienia
#### Tragedia wojny
Eurypides przedstawia wojnę z&nbsp;innej perspektywy niż Homer w&nbsp;[_Iliadzie_](/klasyki/homer-iliada/).
Homer skupił się głównie na walce i&nbsp;śmierci wojowników, podczas gdy Eurypides ukazuje przede wszystkim niedole kobiet i&nbsp;dzieci zdobytej Troi.
Wiele kobiet zostaje brankami.
Andromacha zauważa konflikt związany z&nbsp;faktem nowego zamążpójścia i&nbsp;trudnością pogodzenia się z&nbsp;taką sytuacją.

> [...] Jeśli o&nbsp;Hektorze  
> Najdroższym zapomniawszy, serce swe otworzę  
> Nowemu małżonkowi, wzdyć się sprzeniewierzę  
> Tamtemu; jeśli wiary dochowam mu szczerze,  
> To ten mnie znienawidzi, dziś mój pan na świecie!

#### Obraz Greków
W&nbsp;[_Iliadzie_](/klasyki/homer-iliada/) otrzymujemy w&nbsp;miarę spójny obraz Greków jako prawych wojowników i&nbsp;mądrych władców.
W&nbsp;_Trojankach_ obraz ten jest zmieniony, a&nbsp;pośrednio pokazane są okrucieństwa, jakich dopuścili się Grecy.
Splądrowanie świątyni i&nbsp;gwałt na Kasandrze spowodowały niechęć Ateny i&nbsp;chęć zemsty.
Najbardziej sypie się obraz Odyseusza - z&nbsp;prawego i&nbsp;mądrego władcy nie pozostało nic.


> Łotr to, wróg prawa,  
> Wszystkiemu na poprzek stawa!  
> Chytrych podstępów tkacz,  
> Fałszerz i&nbsp;oszust, co ludzi  
> Językiem dwoistym judzi,  
> Co sieje waśnie,  
> Przyjaźń w&nbsp;nieprzyjaźń zamienia! [słowa Hekabe na temat Odyseusza]

Odyseusz jest przedstawiony jako oszust, podobny do Żmijowego Języka z&nbsp;_Władcy Pierścieni_.
Nic tylko sieje ferment i&nbsp;skazuje dzieci na śmierć, bo to on wymyślił plan zgładzenia syna Andromechy!

> TALTHYBIOS.  
> Gdy powiem: Syn twój umrze!, serce ci zakrwawię.  
> ANDROMACHE.  
> Największe to nieszczęście, jakie mnie dotknęło!  
> TALTHYBIOS.  
> Na wszechnaradzie Greków Odyssa to dzieło.  
>  ANDROMACHE.  
> O&nbsp;raty! O&nbsp;przeraty! Dokądże to zmierza?!  
>  TALTHYBIOS.  
> Powiedział: »Precz ze synem pierwszego rycerza!«  
>  ANDROMACHE.  
> »Trza strącić go« — powiedział — »z wyniosłych baszt Troi!«

W&nbsp;oczach Hekabe postępowanie Greków w&nbsp;stosunku do Astyanaksa zasługuje na potępienie i&nbsp;nie przystoi dzielnym wojownikom.

> [...] Dzielni wy, Achaje,  
> Do bitwy, ale mniej wam rozumu dostaje:  
> Czy mord ten niesłychany spełniliście z&nbsp;trwogi  
> Przed chłopcem, by nie urósł i&nbsp;Troi mej drogiej  
> Z&nbsp;upadku nie podźwignął? Więc byliście niczem,  
> Kiedyśmy tak przed waszem ginęli obliczem.

## Bibliografia
\[Eurypides\] Eurypides.
*Trojanki*.
Akademia Umiejętności, 1918.
[Link do książki w&nbsp;Wikiźródła](https://pl.wikisource.org/wiki/Trojanki_%28Eurypides%2C_t%C5%82um._Kasprowicz%2C_1918%29)
