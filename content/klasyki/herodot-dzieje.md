---
title: Herodot - Dzieje
date: 2025-02-23T12:30:00
---

_Dzieje_ Herodota to tekst historyczno-etnograficzny, w&nbsp;którym autor opisuje wojny i&nbsp;podboje czterech pokoleń perskich władców: Cyrusa, Kambizesa, Dariusza i&nbsp;Kserksesa. Jednocześnie zawiera liczne informacje o&nbsp;geografii i&nbsp;kulturze podbijanych ludów.

Struktura _Dziejów_ przypomina drzewo – głównym tematem są wojny perskie, a&nbsp;liczne rozgałęzienia (dygresje), często wielopoziomowe, dotyczą różnych pobocznych zagadnień. Co ciekawe, Herodot często przedstawia kilka wersji tej samej opowieści. Przykładem może być historia pochodzenia Scytów (księga IV 5-12), której podaje on aż trzy różne warianty.

Herodot poszukuje przyczyn wojen prowadzonych przez Persów, które często nie są jednoznaczne. Ma skłonność do wyjaśniania konfliktów poprzez ludzkie emocje. Na przykład Krezus zaatakował Egipt z&nbsp;powodu gniewu na Egipcjan, a&nbsp;Dariusz wyruszył przeciwko Scytom, by ich ukarać.

## Podział tekstu
Trudno jednoznacznie podzielić _Dzieje_ Herodota na części ze względu na wielość poruszanych w&nbsp;nich tematów. Choć autor sam podzielił swoje dzieło na dziewięć ksiąg, nie jest to podział idealny. Lepszym rozwiązaniem byłoby uporządkowanie tekstu według jednego z&nbsp;dwóch kluczy:  

**1. Według opisywanych władców Persji:**  
- Cyrus – Księga I&nbsp; 
- Kambizes – Księga II, Księga III&nbsp; 
- Dariusz – Księga III, Księga IV, Księga V, Księga VI, Księga VII&nbsp; 
- Kserkses – Księga VII, Księga VIII, Księga IX  

**2. Według podbojów Persów:**  
- Podbój Azji Mniejszej – Księga I&nbsp; 
- Podbój Egiptu – Księga II, Księga III&nbsp; 
- Podbój Scytów – Księga IV  
- Podbój Libii – Księga IV  
- Podbój Tracji i&nbsp;Macedonii – Księga V  
- Wojny persko-greckie – Księga VI, Księga VII, Księga VIII, Księga IX  

## Bibliografia
\[Herodot\] Herodot. _Dzieje_. Spółdzielnia Wydawnicza "Czytelnik", Warszawa, 2020 [Link do książki w&nbsp;Amazon.pl](https://www.amazon.pl/DZIEJE-HERODOT-wyd-12-Herodot/dp/8307036046)
