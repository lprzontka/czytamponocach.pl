---
title: Hezjod - Theogonia
date: 2024-01-28T12:30:00
---

Nie mogę powiedzieć, że _Theogonia_ Hezjoda przypadła mi do gustu.
Utwór nie jest napisany w&nbsp;przystępny sposób, a&nbsp;archaiczne tłumaczenie tylko utrudnia jego zrozumienie.
Tekst jest chaotyczny, pełen dygresji i&nbsp;długich wyliczeń nazw własnych, przez co trudno utrzymać skupienie.
Osoba, która nie zna dobrze mitologii greckiej, może mieć spore problemy z&nbsp;przyswojeniem tej lektury.
Niestety, dla mnie była to raczej męcząca niż satysfakcjonująca książka.

## O&nbsp;treści
### W&nbsp;jednym zdaniu
_Theogonia_  opowiada o&nbsp;powstaniu świata i&nbsp;bogów greckich, ich genealogii i&nbsp;walkach o&nbsp;władzę, jakie prowadzili.

### Najważniejsze historie zawarte w&nbsp;Theogoni:
  * sukcesja Kronosa i&nbsp;okaleczenie Uranosa;
  * pożeranie dzieci przez Kronosa i&nbsp;narodziny Zeusa;
  * dzieje Prometeusza;
  * 10-letnia wojna miedzy bogami olimpijskimi i&nbsp;sturękimi, a&nbsp;tytanami;
  * pokonanie Tyfona i&nbsp;objęcie ostatecznej władzy przez Zeusa.

W&nbsp;*Theogoni* dwukrotnie otrzymujemy historię o&nbsp;obaleniu ojca przez syna w&nbsp;celu objęcia władzy; Kronos obala Urana, a&nbsp;następnie Zeus obala Kronosa.

## Bibliografia
\[Hezjod\] Hezjod.
*Teogonja.* Wacław Maślankiewicz, Warszawa, 1904.
[Link do książki w&nbsp;Wikiźródła](https://pl.wikisource.org/wiki/Teogonja_(Hezjod,_1904))
