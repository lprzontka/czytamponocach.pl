---
title: Homer - Iliada
date: 2024-03-31T12:30:00
---

_Iliada_ Homera to dzieło monumentalne, ale niełatwe w&nbsp;odbiorze.
Tekst jest bardzo długi i&nbsp;momentami nużący, co sprawiło, że nie do końca przypadł mi do gustu.
Dodatkowym utrudnieniem było archaiczne tłumaczenie, które miejscami odbierało płynność lekturze.
Mimo to muszę przyznać, że język _Iliady_ jest niezwykle barwny, pełen rozbudowanych porównań i&nbsp;epickiego rozmachu, co nadaje mu niepowtarzalny charakter, na przykład:

> A&nbsp;jak śmiałe odyńce, kiedy ich psy gonią,  
> Zwracają się i&nbsp;kłami walecznie się bronią:  
> Tak ci zwróceni walczą, dając tym odporem  
> Wytchnienie pierzchającym Grekom przed Hektorem.
[Księga 11, 215-218]

## O&nbsp;treści
### W&nbsp;jednym zdaniu
_Iliada_ opowiada o&nbsp;ostatnich dniach wojny trojańskiej.

### Krótkie streszczenie
Na początku epopei Achilles i&nbsp;Agamemnon zaplątują się w&nbsp;spór, który prowadzi do wycofania się Achillesa z&nbsp;walki.
Decyzja ta prowadzi do tragicznych wydarzeń, obejmujących niemalże klęskę Greków oraz śmierci Patroklesa.
W&nbsp;rezultacie Achilles mści się na Hektorze, zabijając go.
Następnie oddaje ciało Hektora jego ojcu, Priamowi, i&nbsp;zapewnia Trojanom dwanaście dni na żałobę.

### Główne zagadnienia

#### Heroizm
Cała Iliada ukazuje czyny starożytnych herosów, takich jak Ajas, Hektor, Menelaj, Odyseusz i&nbsp;wielu innych, którzy brali udział w&nbsp;wojnie.
W&nbsp;większości są pokazywani na polu bitwy, gdzie króluje męstwo i&nbsp;odwaga.
Wszelkie przejawy tchórzostwa są potępiane, na przykład:
* Parys zostaje zganiony przez swojego brata, Hektora, gdy próbuje ukryć  się przed Menelajem:

> Podły nikczemniku,  
> Zręczny tylko do niewiast, chytry płci zwodniku,  
> Z&nbsp;kształtu twoja zaleta. Obyś się nie rodził,  
> Obyś był nigdy w&nbsp;śluby małżeńskie nie wchodził!  
> I&nbsp;mnie by lepiej było, i&nbsp;dla ciebie z&nbsp;zyskiem,  
> Nie byłbyś się stał ludzkim, jak dzisiaj, igrzyskiem.  
> Jak tam szydzą Achaje, którym twoja postać  
> Wróżyła, że potrafisz śmiało placu dostać;  
> Lecz nie masz w&nbsp;sercu męstwa, nie masz w&nbsp;duszy cnoty.  
> A&nbsp;więc tak nikczemnymi obdarzon przymioty  
> Jakże śmiałeś na morze spuszczać twoje nawy?  
> Jak śmiałeś, lud zebrawszy do niecnej wyprawy,  
> Śliczną dziewkę oderwać od związków weselnych?  
> Wykraść niewiastę, krewną bohaterów dzielnych?  
> Skąd szkoda ojcu, państwu zguba ostateczna,  
> Radość nieprzyjaciołom, tobie hańba wieczna.  
> Czemuś się na Atryda nie wziął do oręża?  
> Doświadczyłbyś, jakiego żonę trzymasz męża.  
> W&nbsp;upominkach Kiprydy próżnej szukasz chluby,  
> Lutnia, kształt, włos trefiony nie wyrwie od zguby.  
> Lecz i&nbsp;Trojanie podli, że ciebie w&nbsp;zapłatę  
> Nieszczęść tylu w&nbsp;kamienną nie odziali szatę.
[Księga 3, 39-60]

* Diomed sprzeciwia się Agamemnonowi, gdy ten proponuje odwrót i&nbsp;opuszczenie Troi:

> Nie miej za złe, że płochej sprzeciwiam się mowie  
> Jeśli sam chcesz powracać, powracaj w&nbsp;tej porze,  
> Nawy twe bliskie brzegów, otwarte jest morze;  
> Drudzy zostaną Grecy, aż Troi dobędą.  
> Jeśli i&nbsp;ci chcą wrócić, niech na nawy wsiędą,  
> Niech płyną; my haniebnej ucieczki nie dzielem  
> I&nbsp;póki będziem walczyć oba ze Stenelem,  
> Aż dumne wywrócimy Ilijonu grody,  
> Bośmy tu za boskimi przybili powody [Księga 9, 28-36]

#### Tragedia wojny
Tragedia w&nbsp;_Iliadzie_ to nie tylko śmierć Patroklesa i&nbsp;Hektora, których opłakiwali odpowiednio Achilles i&nbsp;Pryjam.
To przede wszystkim tragedia zwykłych żołnierzy na polu bitwy.
Wielu bezimiennych wojowników ginie na kartach tej historii, jednak krótki opis śmierci Akemasa, jedynego syna Forbasa, dobitnie ukazuje ludzką niedolę na wojnie:

> Lecz najbardziej się mężny Penelej zapalił;  
> Leci na Akamasa, ale ten się chroni:  
> Zgubny raz Ilijonej dostał z&nbsp;jego dłoni.  
> Ojciec, Forbas, największe stada tam posiadał,  
> Hermejas go pokochał, bogactwami nadał,  
> A&nbsp;tego tylko żona powiła mu syna.  
> Zgasła twa, biedny starcze, pociecha jedyna!  
> Trafił w&nbsp;oko, źrzenicę ruszył grot stalony,  
> Przeszedł na wylot głowę. Rycerz, krwią zbroczony,  
> Wyciąga obie ręce i&nbsp;na ziemi siada;  
> Wtedy z&nbsp;mieczem dobytym Penelej przypada,  
> Tnie w&nbsp;szyję: głowa z&nbsp;włócznią, którą jest przebita,  
> I&nbsp;z&nbsp;przyłbicą upada. [Księga 14, 267-279]

#### Przeznaczenie
Bogowie w&nbsp;Iliadzie odgrywają kluczową rolę, rządząc losem każdego człowieka.
Wiele razy ingerują w&nbsp;wydarzenia, co jest wyraźnie podkreślane, na przykład podczas śmierci Archelocha, czy w&nbsp;słowach samego Zeusa dotyczących śmierci Sarpedona:

> Polidam śmierci zręcznym uchronił się zwrotem,  
> Lecz za to Archelocha cios dosięgnął srogi,  
> Dla niego zgubę mściwe przeznaczyły bogi.   
> W&nbsp;kark go utrafia dzida Telamona syna,  
> Okrutnym razem żyły obiedwie podcina.   
> Padł i&nbsp;ziemi, waląc się pod śmiertelnym ciosem,  
> Wprzód niż kolanem dotknął czołem, twarzą, nosem. [Księga XIV, 241-247]

> Zbliża się smutna chwila przeznaczona losem,  
> W&nbsp;której ma lec Sarpedon pod Patrokla ciosem. [Księga XVI, 243-244]

Bogowie wpływają na przebieg bitwy według własnego upodobania.
Posejdon, Atena czy Apollo biorą nawet czynny udział w&nbsp;walkach.

> Wtedy Zeus na górnej Idzie przebudzony,  
> Wyrwał się z&nbsp;towarzystwa pięknej swojej żony,  
> Wstał, na Greków i&nbsp;Trojan boskim rzucił okiem:  
> Ci pierzchają, ci mężnym następują krokiem;  
> Posejdon tam achajskiej przywodzi młodzieży;  
> Hektor wśród towarzyszów osłabiony leży,  
> Silną ranion prawicą, krew z&nbsp;piersi wyrzuca  
> I&nbsp;ledwie mu tchem słabym oddychają płuca.   
> Zabolał ojciec bogów nad rycerza losem… [Księga XV, 5-13]

#### Duma
_Iliada_ rozpoczyna się od sporu Agamemnona i&nbsp;Achillesa, który bierze się z&nbsp;urażonej królewskiej dumy.

> Lecz wiedz: gdy Feb ode mnie Chryzeidę bierze,  
> Dam rozkaz, niech ją moi odwiozą żołnierze,  
> A&nbsp;za to się do twojej posunę nagrody:  
> Bryzeidę ci wezmę z&nbsp;pięknymi jagody,  
> Byś znał, żem jest mocniejszy. [Księga I, 185-189]

Achilles również przez swoją urażoną dumę prosi matkę o&nbsp;wstawiennictwo u&nbsp;Zeusa, aby ten wspomógł Trojan.

> Wspomnij mu to, kolana w&nbsp;czułej ściskaj dłoni,  
> Żebraj, aby trojańskiej dopomagał broni;  
> Niech bije Greki, do naw odeprze ze stratą,  
> Niech widzą, co mądrości ich króla zapłatą,  
> I&nbsp;niechaj Atryd swego pożałuje błędu,  
> Że dla najdzielniejszego męża nie miał względu [Księga I, 375-380]

Obydwoje nie dają się ubłagać, jednak Agamemnon zastanawia się nad swoim postępowaniem, gdy zaczyna przegrywać wojnę z&nbsp;powodu woli Zeusa.
Pomimo przeprosin ze strony króla, Achilles nadal nie rusza na pole bitwy.
Dopiero po śmierci Patroklesa, Achilles pokonuje swoją dumę i&nbsp;mści się na Hektorze.

## Bibliografia
\[Homer\] Homer.
_Iliada_.
Fundacja Nowoczesna Polska, 2022.
[Link do książki w&nbsp;WolneLektury.pl](https://wolnelektury.pl/katalog/lektura/homer-iliada/)
