---
title: Homer - Odyseja
date: 2024-04-28T12:30:00
---

_Odyseja_ Homera, podobnie jak [_Iliada_](/klasyki/homer-iliada/), jest tekstem długim i&nbsp;momentami nużącym.
Mimo to podobała mi się bardziej niż [_Iliada_](/klasyki/homer-iliada/), choć archaiczne tłumaczenie ponownie utrudniało odbiór.
Ku mojemu zaskoczeniu, samym przygodom Odyseusza poświęcono stosunkowo niewiele miejsca – większa część utworu koncentruje się na sytuacji w&nbsp;jego domu oraz późniejszym pogromie biesiadników.
Spodziewałem się czegoś innego, ale i&nbsp;tak była to ciekawa lektura.

## O&nbsp;treści
### W&nbsp;jednym zdaniu
_Odyseja_ opowiada o&nbsp;podróży Odyseusza do Itaki po zakończonej wojnie trojańskiej, powrocie do domu i&nbsp;rozprawieniu się z&nbsp;zalotnikami Penelopy.

### Zarys fabuły
Po podbiciu Ismar w&nbsp;drodze powrotnej z&nbsp;Troi, wojska Odyseusza zostają zaatakowane i&nbsp;muszą uciekać na morze.
Odyseusz wraz z&nbsp;towarzyszami trafili na wyspę cyklopów, gdzie okaleczyli Polifema, aby ujść z&nbsp;życiem z&nbsp;jego pułapki.
Polifem przeklina Odysa:

> Usłysz mnie, Ziemiotrzęsco, usłysz, czarnogrzywy!  
> Jeślim syn twój, a&nbsp;tyś jest rodzic mój prawdziwy,  
> I&nbsp;Ścigaj tego Odysa, zakaż nieść go wodom  
> Do Itaki, i&nbsp;nigdy niech nie wróci do dom!  
> A&nbsp;choćby przeznaczenie oglądać mu dało  
> Ziemię ojców, dom własny i&nbsp;w nim pozostałą  
> Rodzinę, to najpóźniej, po tułaczce długiej,  
> Jak nędzarz, bez nikogo z&nbsp;drużyny, bez sługi,  
> Na niewłasnym okręcie niech wróci do domu  
> I&nbsp;zastanie we własnym gnieździe pełno sromu!  [Pieśń IX, 555-564]

Następnie Odyseusz z&nbsp;załogą tułał się po morzach, aż na sam koniec trafił na wyspę ze stadami wołów na cześć Heliosa.
Po zabiciu wołów, zostali oni przeklęci, a&nbsp;statek został zatopiony na morzu.
Jedynie Odyseusz się uratował i&nbsp;trafił na wyspę Ogygia, którą zamieszkiwała nimfa Kalipso, i&nbsp;spędził tam siedem lat.

Telemach, natchnionych przez Atenę, wyrusza w&nbsp;podróż do Pylos i&nbsp;Sparty, aby zdobyć informację o&nbsp;swoim ojcu.
W&nbsp;tym samym czasie Odyseusz opuszcza Kalipso i&nbsp;udaje się w&nbsp;podróż do Itaki.
Oboje wracają do domu, gdzie panoszą się biesiadnicy i&nbsp;zalotnicy Penelopy.
Odyseusz wraz z&nbsp;Telemachem rozprawiają się z&nbsp;gachami.

### Główne zagadnienia
Jakie są główne tematy opowieści, o&nbsp;czym ona jest? Np, o&nbsp;miłości, o&nbsp;bohaterstwie.

#### Gościnność
Gościnność jest powtarzanym motywem w&nbsp;_Odysei_.
Poprzez liczne przygody, Odyseusz (oraz Telemach) wielokrotnie zostają na łasce swoich gospodarzy.

> „Jacyś tam obcy ludzie, o&nbsp;mój jasny panie!  
> Jest ich dwóch; wyglądają iście jak niebianie!  
> Rozkaż, czy wyprząc konie mamy im, czy dalej  
> Odprowadzić, by indziej gościny szukali?”  
> Menelaj tym oburzon rzekł: „Synu Boety  
> Eteoneju! Gadasz, jak gdyby to nie ty,  
> Tylko dziecko, co głupie powtarza wyrazy!  
> Czyż to my gościnności ludzkiej tyle razy  
> Nie doznali obadwa przed naszym powrotem?  
> Zeus mógłby znów nas karać, gdyby zabyć o&nbsp;tem.  
> Śpiesz się więc wyprząc konie i&nbsp;dać im wygody,  
> A&nbsp;tych gości zaprosić na weselne gody”. [Pieśń IV, 27-38]

Sposób w&nbsp;jaki Menelaj podejmuje Telemacha, czy gościnność Feaków udzielana Odyseuszowi, są przykładami tego, jak należy traktować gości, którzy zawsze są pod opieką Zeusa.

> Grzech byłby i&nbsp;biedniejszym niźli ty żebrakiem  
> Gardzić i&nbsp;w próg nie puszczać. Tułacz czy ubogi,  
> Jest pod Zeusa opieką. [Pieśń XIV, 56-58]

Cyklop Polifem i&nbsp;nimfa Kalipso byli przykładami negatywnymi.
Nie dbali o&nbsp;podstawowe zasady gościnności.
Nimfa więziła Odyseusza, a&nbsp;cyklop chciał zjeść wszystkich, zostawiając Odyseusza (który przedstawił się jako Nikt) na sam koniec.

> Słuchaj, bratku!  
> Nikt zjedzon będzie; jednak zjem go na ostatku,  
> A&nbsp;tych tam pierwej pożrę — ot, masz podarunek! [Pieśń IX, 387-389]

Zalotnicy w&nbsp;domu Penelopy nadużywali prawa gościnności.
Prowadzi to do tragicznych wydarzeń i&nbsp;konfrontacji z&nbsp;Odyseuszem po jego powrocie.


#### Walka z&nbsp;przeciwnościami losu
Bogowie mają wpływ na los Odyseusza, podobnie jak miało to miejsce w&nbsp;[_Iliadzie_](/klasyki/homer-iliada/).
Odys aktywnie starał się walczyć z&nbsp;przeciwnościami losu.
Wszystkie jego przygody wynikały z&nbsp;silnej chęci powrotu do ojczyzny i&nbsp;domu rodzinnego, a&nbsp;w szczególności do Penelopy.


#### Miłość
_Odyseja_ jest piękną opowieścią o&nbsp;miłości.
Odyseusz tęsknił z&nbsp;Penelopą nawet gdy był uwięziony u&nbsp;Kalipso.

> [...] o&nbsp;nimfo! Jaż bym to nie wiedział,  
> Że między Penelopą a&nbsp;tobą jest przedział?  
> Że się równać nie może z&nbsp;twym wzrostem, urodą?  
> Prosta to śmiertelniczka, a&nbsp;tyś wiecznie młodą!  
> Mimo to tęsknię do niej; tęsknię coraz więcej,  
> Aby się dnia powrotu doczekać najpręcej;  
> Choćby mnie bóg znów jaki pogrążył w&nbsp;bałwany,  
> Zniosę mężnie, w&nbsp;cierpieniach jam wypróbowany.  
> Tylem już przebył, takie przechodził katusze  
> Na wojnie i&nbsp;na morzu, że i&nbsp;to znieść muszę! [Pieśń V, 218-227]

Penelopa również czyniła wszystko, bo kochała swojego męża i&nbsp;wierzyła, że wróci on do niej cały i&nbsp;zdrowy.
Przy każdej okazji pytała o&nbsp;swojego męża.

> Synu! Mamże do siebie na górę tam wrócić  
> I&nbsp;znowu się na łoże łzami zlane rzucić,  
> Które wylewam, odkąd wraz z&nbsp;Atreja syny  
> Mąż mój poszedł na Ilion? Czemuż, mój jedyny  
> Nie chcesz, nim się tu zejdą ci zuchwali gburzy,  
> Opowiedzieć o&nbsp;ojcu, coś słyszał w&nbsp;podróży? [Pieśń XVII, 104-109]

Nawet gdy Odyseusz wrócił do domu, Penelopa nie była ufna.
Dopiero gdy Odyseusz ujawnił jej ich wspólną tajemnicę, doszło do ich pojednania.

## Bibliografia
\[Homer\] Homer.
_Odyseja_.
Fundacja Nowoczesna Polska, 2022.
[Link do książki w&nbsp;WolneLektury.pl](https://wolnelektury.pl/katalog/lektura/homer-odyseja/)
