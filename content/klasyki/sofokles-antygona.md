---
title: Sofokles - Antygona
date: 2024-11-24T12:30:00
---

_Antygona_ to dramat Sofoklesa, który opowiada o&nbsp;tragicznym losie tytułowej bohaterki. Kreon, władca Teb, zabrania pochówku Polinika, brata Antygony, który zginął w&nbsp;bratobójczej walce o&nbsp;władzę. Antygona, nie mogąc pogodzić się z&nbsp;bezczeszczeniem ciała zmarłego, sprzeciwia się królewskiemu zakazowi i&nbsp;dokonuje pochówku, za co zostaje skazana na śmierć. Hajmon, syn Kreona i&nbsp;narzeczony Antygony, nie wyobraża sobie bez niej życia. Po samobójczej śmierci Antygony Hajmon również odbiera sobie życie, co prowadzi do kolejnej tragedii – śmierci Eurydyki, żony Kreona i&nbsp;matki Hajmona.

## Protest przeciw tyranii
_Antygona_ ukazuje tyranię Kreona oraz sprzeciw tytułowej bohaterki wobec jego rządów. Władca Teb, w&nbsp;rozmowie z&nbsp;synem, podkreśla wagę posłuszeństwa obywateli wobec władzy:

> [...] Kto zaś zuchwale przeciw prawu działa  
> I&nbsp;tym, co rządzą, narzucać chce wolę,  
> Ten nie doczeka się mego uznania.  
> Wybrańcom ludu posłusznym być trzeba  
> W&nbsp;dobrych i&nbsp;słusznych, nawet w&nbsp;innych sprawach. 

Kreon zabronił pochówku Polinika, argumentując, że jego zdrada – próba przejęcia władzy w&nbsp;Tebach siłą – zasługuje na najwyższe potępienie. Jednakże, w&nbsp;swoim dążeniu do karania zdrajcy, Kreon narusza boskie prawo nakazujące grzebanie zmarłych.

Antygona, nie godząc się na taki rozkaz, otwarcie sprzeciwia się Kreonowi. Jej działania motywuje głęboka wiara w&nbsp;boskie nakazy i&nbsp;poczucie moralnego obowiązku:

> ISMENA&nbsp; 
> Więc ty zamierzasz grzebać wbrew ukazom?  
>  
> ANTYGONA&nbsp; 
> Tak! Brata mego, a&nbsp;dodam… i&nbsp;twego;  
> Bo wiarołomstwem nie myślę się kalać. 

Antygona jest gotowa zaryzykować własne życie, by uczynić to, co uważa za słuszne i&nbsp;sprawiedliwe.

## Autorytaryzm
Dramat podejmuje również problematykę władzy i&nbsp;jej relacji z&nbsp;obywatelami. Czy władca powinien służyć swoim poddanym, czy to raczej obywatele mają być podporządkowani władcy? W&nbsp;rozmowie Kreona z&nbsp;Hajmonem padają słowa, które kwestionują absolutystyczne podejście do rządzenia. 

> HAJMON  
> Marne to państwo, co li panu służy.
>  
> KREON  
> Czyż nie do władcy więc państwo należy?
>  
> HAJMON  
> Pięknie byś wtedy rządził… na pustyni. 

Autorytaryzm Kreona ostatecznie prowadzi do tragedii, skutkując śmiercią jego syna i&nbsp;żony.

## Bibliografia
\[Sofokles\] Sofokles. *Antygona*. Fundacja Nowoczesna Polska, 2022. [Link do książki w&nbsp;WolneLektury.pl](https://wolnelektury.pl/katalog/lektura/antygona/)
