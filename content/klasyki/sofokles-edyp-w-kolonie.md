---
title: Sofokles - Edyp w Kolonie
date: 2024-10-27T12:30:00
---

_Edyp w&nbsp;Kolonie_ jest dramatem Sofoklesa, który opowiada o&nbsp;ostatnich dniach życia Edypa. Po długiej wędrówce, Edyp, w&nbsp;towarzystwie swojej córki Antygony, przybywa do Kolonos, gdzie szuka schronienia i&nbsp;spokoju w&nbsp;świętym gaju. Prosi króla Aten, Tezeusza, o&nbsp;azyl, ponieważ zgodnie z&nbsp;przepowiednią miejsce jego pochówku przyniesie błogosławieństwo. Tymczasem Kreon i&nbsp;Polinik próbują wykorzystać Edypa dla własnych korzyści, lecz on ich przeklina. Następnie Edyp udaje się z&nbsp;Tezeuszem w&nbsp;odosobnione miejsce, gdzie umiera w&nbsp;tajemnicy, zapewniając Atenom boską ochronę.

## Próba usprawiedliwienia win
W&nbsp;dramacie Edyp przyznaje się do popełnionych zbrodni, lecz próbuje je usprawiedliwić, podczas gdy w&nbsp;[_Królu Edypie_](/klasyki/sofokles-krol-edyp/) bez sprzeciwu przyjął swoją karę.

> CHÓR.  
> Zabiłeś!
> 
> EDYP.  
> Tak, lecz mogę nieco…  
> 
> CHÓR.  
> Cóż takiego?  
>  
> EDYP.  
> Się uniewinnić.  
> 
> CHÓR.  
> Czem?  
>  
> EDYP.  
> Wnet ci powiem.  
> Tych, co wygubić mnie chcieli, zabiłem,  
> Prawem to czysty, bezwiednie spełniłem.

Edyp tłumaczy, że jego czyny nie były wynikiem świadomego działania:

> Zniosłem ja straszne, o&nbsp;straszne ja czyny,  
> Lecz bogi wiedzą, nie mojej to winy,  
> Nie mojej woli to dzieło.

W&nbsp;rozmowie z&nbsp;Kreonem ponownie podkreśla nieumyślność swoich zbrodni:

> Lecz to wiem jedno, że z&nbsp;woli ty własnej  
> Mnie lżysz i&nbsp;tamtą, a&nbsp;jam ją wbrew woli  
> Pojął i&nbsp;sprawę wbrew woli wspominam.  
> Ale zaprawdę, ni w&nbsp;związkach tych nawet  
> Złym się nie wydam, ni w&nbsp;ojca morderstwie,  
>  Które mi ciągle w&nbsp;twarz miotasz z&nbsp;obelgą.

Edyp przypomina, że jego nieszczęścia były rezultatem interwencji bogów, a&nbsp;nie jego świadomych decyzji:

> Ja więc popadłem w&nbsp;te klęski z&nbsp;dopustu  
> Bogów, a&nbsp;mniemam, że nawet duch ojca,  
> By żyw był jeszcze, nie przeczyłby temu.

## Bibliografia
\[Sofokles\] Sofokles. *Król Edyp*. Fundacja Nowoczesna Polska, 2022. [Link do książki w&nbsp;Wikiźródła](https://pl.wikisource.org/wiki/Edyp_w_Kolonie_(Sofokles,_t%C5%82um._Morawski,_1916))
