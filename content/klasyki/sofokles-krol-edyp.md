---
title: Sofokles - Król Edyp
date: 2024-08-25T12:30:00
---

_Król Edyp_ to dramat Sofoklesa, który opowiada historię Edypa, króla Teb. W&nbsp;czasie zarazy nawiedzającej miasto, Edyp przeprowadza dochodzenie, aby znaleźć zbójcę poprzedniego króla, Lajosa. Zbrodniarz musi zostać ukarany, aby zaraza ustała. W&nbsp;trakcie dochodzenia wychodzi na jaw, że to sam Edyp jest mordercą Lajosa, swojego ojca. Z&nbsp;tego powodu skazuje on się na ślepotę i&nbsp;opuszcza miasto.

## Przeznaczenie
> Niech więc się moje spełnia przeznaczenie!

Historia Edypa jest opowieścią o&nbsp;nieuchronnym losie. Pokazuje, że nie można przed nim uciec, a&nbsp;wszelkie próby jego zmiany okazują się daremne. Przeznaczenie dotyka Edypa, jego matki Jokasty, jak i&nbsp;ojca Lajosa. Cała trójka za wszelką cenę chciała zmienić swój los. Lajos postanowił uśmiercić swego syna, lecz to mu się nie udało.

> Wiedz więc, że Lajos otrzymał był wróżby,  
> Nie od Apolla, lecz od jego służby,  
> Iż kiedyś śmierć go z&nbsp;rąk syna pokona,  
> Co zeń zrodzony — i&nbsp;z mojego łona.  
> A&nbsp;wszakże jego, jak wieść niesie, obce  
> Zabiły zbiry w&nbsp;troistym rozdrożu.  
> A&nbsp;to niemowlę, gdy trzeci dzień świtał,  
> Przebiwszy u&nbsp;nóg kosteczki, wysadził  
> On ręką obcą gdzieś w&nbsp;górskich ostępach.

Edyp, wychowany w&nbsp;Koryncie, wyruszył do wyroczni w&nbsp;Delfach. Tam uzyskał straszliwą przepowiednię i&nbsp;postanowił nie wracać do domu. Jednak w&nbsp;drodze zabił Lajosa, swojego ojca, i&nbsp;tym samym spełnił przepowiednię.

> [...] lecz straszne  
> Za to mi inne wypowiedział wróżby,  
> Że matkę w&nbsp;łożu ja skalam, że spłodzę  
> Ród, który ludzi obmierznie wzrokowi,  
> I&nbsp;że własnego rodzica zabiję.

Trafiwszy do Teb, Edyp nieświadomie poślubił swoją matkę i&nbsp;w ten sposób wypełnił przepowiednię dotyczącą swojego losu.

## Bibliografia
\[Sofokles\] Sofokles. *Król Edyp*. Fundacja Nowoczesna Polska, 2022. [Link do książki w&nbsp;WolneLektury.pl](https://wolnelektury.pl/katalog/lektura/krol-edyp/)
